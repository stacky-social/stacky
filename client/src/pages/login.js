import React, { useState, useEffect } from "react";
import { Link, useHistory } from "react-router-dom";
import { login } from "../redux/actions/authAction";
import { useDispatch, useSelector } from "react-redux";
import Input from "../components/shared/Input";
import Button from "../components/shared/Button";
import { ReactComponent as Logo } from '../assets/icons/logo.svg'

const Login = () => {
  const initialState = { email: "", password: "" };
  const [userData, setUserData] = useState(initialState);
  const { email, password } = userData;
  const { auth } = useSelector((state) => state);
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    if (auth.token) history.push("/");
  }, [auth.token, history]);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setUserData({ ...userData, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(login(userData));
  };

  return (
    <div className="auth_page">
      <form onSubmit={handleSubmit}>
        <Logo className="form-logo" />
        <Input
          name="email"
          placeholder="Email"
          value={email}
          onChange={handleInputChange}
        />
        <Input
          type="password"
          name="password"
          placeholder="Password"
          value={password}
          onChange={handleInputChange}
        />
        <Button type="submit" disabled={!(email && password)}>
          Login
        </Button>
      </form>
      <div className="navigate-box">
        <p className="text">
          Don't have an account?&nbsp;&nbsp;
          <Link to="/register">
            Sign up
          </Link>
        </p>
      </div>
    </div>
  );
};

export default Login;
