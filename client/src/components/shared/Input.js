import React from "react";
import './styles.scss'

const Input = ({ name, type, value, onChange, label, classes, placeholder }) => {
  return (
    <div className="input-wrapper">
      {label && <label htmlFor={name}>{label}</label>}
      <input
        name={name}
        id={name}
        type={type}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        className={`input-wrapper__input ${classes}`}
      />
    </div>
  );
};

Input.defaultProps = {
  type: "text",
  label: null,
  placeholder: '',
  classes: ''
};

export default Input;
