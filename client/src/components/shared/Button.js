import React from "react";
import './styles.scss';

const Button = ({ type, disabled, classes, contentClasses, children, onClick }) => {
  console.log(disabled)
  return (
    <button
      type={type}
      onClick={onClick}
      className={`button ${classes} ${disabled && 'disabled'}`}
      disabled={disabled}
    >
      <div className={`button__content ${contentClasses}`}>
        {children}
      </div>
    </button>
  );
};

Button.defaultProps = {
  type: "button",
  disabled: false,
  classes: '',
  contentClasses: '',
  onClick: null,
};

export default Button;
