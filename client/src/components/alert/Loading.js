import React from "react";
import { ReactComponent as LoadingIndicator } from "../../assets/icons/loading.svg";

const Loading = () => {
  return (
    <div
      className="position-fixed w-100 h-100 text-center loading"
      style={{
        backgroundColor: "rgba(255, 255, 255, 0.3)",
        color: "white",
        top: 0,
        left: 0,
        zIndex: 50,
      }}
    >
      <LoadingIndicator />
    </div>
  );
};

export default Loading;
